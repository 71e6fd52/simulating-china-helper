#!/usr/bin/env ruby
# frozen_string_literal: true

require 'active_support/all'
require 'date'
require 'telegram/bot'
require 'distribution'

require './const'
require './parse'

def time_r2s(real)
  days = real.to_datetime - START
  year = days.floor / 12 + 2100
  mo = days.floor % 12 + 1
  day = days % 1 * Time.days_in_month(mo, year)
  DateTime.new(year, mo, 1) + day
end

def time_s2r(sim)
  sim = sim.to_date
  year = sim.year - 2100
  mo = sim.mon - 1
  days = sim.mday.to_f / Time.days_in_month(mo, year) + mo + year * 12
  START + days
end

def time_r2s2(real)
  START2[1] + (real.to_date - START2[0])
end

def time_s2r2(sim)
  START2[0] + (sim.to_date - START2[1])
end

def dice_parse(args)
  args = [1, 6] if args.empty?
  args = args.first.split('d') if args.first =~ /^\d+d\d+$/
  args = [1, args.first] if args.size == 1
  raise 'Unknow dice format' if args.size != 2

  dice = args.map(&:to_i)
  Array.new(dice[0]) { rand 1..dice[1] }.sum
end

token = File.open('token', &:read).chomp

Telegram::Bot::Client.run(token, logger: Logger.new($stderr)) do |bot|
  @logger = bot.logger
  @logger.info 'Bot has been started'

  dp = Dispatcher.new(bot.api) do
    on 'start' do
      send(['主群：https://t.me/joinchat/IlEZXE1atxURMCseb5hF3w',
            'II主群：https://t.me/joinchat/FP3nTxbZjtluIrIWwnl1xg'].join("\n"))
    end

    on 'source' do
      reply('https://gitlab.com/71e6fd52/simulating-china-helper')
    end

    on 'now' do
      send(time_r2s(Time.now).strftime('现在是 %Y 年 %m 月 %d 日 %R'))
    end

    on 'into_sim' do
      time = args.join ' '
      reply(time_r2s(DateTime.parse(time)).strftime('这是 %Y 年 %m 月 %d 日 %R'))
    end

    on 'into_real' do
      time = args.join ' '
      reply(time_s2r(Date.parse(time)).strftime('这是 %Y 年 %m 月 %d 日约 %H 点'))
    end

    on 'now2' do
      send(time_r2s2(Time.now).strftime('今天是 %Y 年 %m 月 %d 日'))
    end

    on 'into_sim2' do
      time = args.join ' '
      reply(time_r2s2(Date.parse(time)).strftime('这是 %Y 年 %m 月 %d 日'))
    end

    on 'into_real2' do
      time = args.join ' '
      reply(time_s2r2(Date.parse(time)).strftime('这是 %Y 年 %m 月 %d 日'))
    end

    on 'normal' do
      arg = args.map(&:to_f)
      reply(Distribution::Normal.rng(*arg).call)
    end

    on 'dice' do
      reply(dice_parse(args))
    end

    on 'rand' do
      reply(rand(*args))
    end

    on 'chat_id' do
      reply(message.chat.id)
    end
  end

  bot.listen do |message|
    time = message.edit_date || message.date
    next if Time.now.to_i - time.to_i > 120

    dp.parse(message)
  rescue Net::OpenTimeout
    sleep 5
    retry
  rescue OpenSSL::SSL::SSLError
    sleep 5
    retry
  rescue StandardError => e
    dp.reply("failed: #{e.message}")
    @logger.error(e.full_message)
  end
end

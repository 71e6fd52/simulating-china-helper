# frozen_string_literal: true

class Command
  attr_accessor :command, :args

  def initialize(command, args)
    @command = command
    @args = args
  end

  def self.parse(str)
    m = str&.match(%r{^/(\S+?)(?:@SimulatingChinaBot)?(?:\s(.+))?$})
    return if m.nil?

    Command.new(m[1], m[2]&.split || [])
  end

  def to_s
    "/#{@command}#{" #{@args.join(' ')}" if @args.any?}"
  end
end

class Dispatcher
  attr_accessor :handles
  attr_reader :message, :api

  def initialize(api, &block)
    @handles = {}
    @api = api
    instance_exec(&block) if block_given?
  end

  def on(command, &block)
    @handles[command] = block
  end

  def parse(message)
    @message = message
    @command = Command.parse(message.text)
    return if @command.nil?

    @handles[command]&.call
  end

  def helper(message, &block)
    @message = message
    @command = Command.parse(message.text)
    instance_exec(&block)
  end

  def command
    @command&.command
  end

  def args
    @command&.args
  end

  def send(text)
    api.send_message(chat_id: message.chat.id, text: text)
  end

  def reply(text)
    api.send_message(chat_id: message.chat.id, text: text,
                     reply_to_message_id: message.message_id)
  end
end

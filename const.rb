# frozen_string_literal: true

require 'date'

START = DateTime.parse('20190214')
START2 = [
  Date.parse('20190314'),
  Date.parse('20181203')
].freeze
PLOT_ID = -1001428866082

#!/usr/bin/env ruby
# frozen_string_literal: true

require 'telegram/bot'
require 'yaml'

require './const'
require './parse'

token = File.open('token-plot', &:read).chomp

EXAMPLE = {
  votes: {
    'message_id' => {
      title: %w[opt1 opt2 opt3],
      data: {
        'user1' => [0.1, 0.5, 0.4],
        'user2' => [0.9, 0.05, 0.05]
      }
    }
  }
}.freeze

def save(data)
  File.open('plot_data', 'w') { |f| f.write YAML.dump(data) }
end

def load
  YAML.load_file('plot_data')
rescue Errno::ENOENT
  { votes: {} }
end

def check(vote, p)
  return '数量错误' unless p.size == vote[:title].size
  return '请确保总和为 1' unless p.sum == 1
  return '任一项的概率不能大于 0.99' unless p.find { |a| a >= 0.99 }.nil?
end

def done(vote)
  avg = vote[:data].values.transpose.map { |a| a.reduce(:+).to_r / a.size }
  range = [0.0]
  avg.each { |a| range << (range.last + a).to_f }
  range.zip(vote[:title]).flatten.tap(&:pop).join(' ')
end

data = load

Telegram::Bot::Client.run(token, logger: Logger.new($stderr)) do |bot|
  @logger = bot.logger
  @logger.info 'Bot has been started'

  dp = Dispatcher.new(bot.api) do
    on 'start' do
      send(['主群：https://t.me/joinchat/IlEZXE1atxURMCseb5hF3w',
            'II主群：https://t.me/joinchat/FP3nTxbZjtluIrIWwnl1xg'].join("\n"))
    end

    on 'vote' do
      if args.empty?
        reply('请输入参数')
        break
      end
      msg = send("回复本条，用空格分隔的 #{args.size} 个数字代表对应项的概率")
      data[:votes][msg['result']['message_id']] = { title: args, data: {} }
      save(data)
    end
  end

  bot.listen do |message|
    next unless message.chat.id == PLOT_ID

    if data[:votes].key? message.reply_to_message&.message_id
      vote = data[:votes][message.reply_to_message&.message_id]
      dp.helper(message) do
        if command == 'done'
          send(done(vote))
          data[:votes].delete(message.reply_to_message&.message_id)
          save(data)
          break
        end
        arg = message.text.split.map(&:to_r)
        err = check(vote, arg)
        unless err.nil?
          reply(err)
          break
        end
        vote[:data][message.from.id] = arg
      end
    else
      dp.parse(message)
    end
  rescue Net::OpenTimeout
    sleep 5
    retry
  rescue OpenSSL::SSL::SSLError
    sleep 5
    retry
  rescue StandardError => e
    dp.reply("failed: #{e.message}")
    @logger.error(e.full_message)
  ensure
    save(data)
  end
end
